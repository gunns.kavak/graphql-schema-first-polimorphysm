
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export interface FinancialUserOldInput {
    id?: string;
    ProfessionalData?: ProfessionalDataOldInput;
    maritalStatus?: MaritalStatusInput;
    residentialAddress?: ResidentialInput;
    maxMonthlyInstallment?: number;
}

export interface ProfessionalDataOldInput {
    income?: number;
}

export interface MaritalStatusOldInput {
    dependents?: number;
}

export interface ResidentialOldInput {
    address?: AddressInput;
}

export interface AddressOldInput {
    zipCode?: string;
    state?: string;
}

export interface FinancialUserInput {
    id?: string;
    userProfileId?: string;
    userId?: number;
    firstName?: string;
    secondName?: string;
    lastName?: string;
    secondLastName?: string;
    gender?: string;
    email?: string;
    phoneNumber?: string;
    birthDate?: string;
    motherName?: string;
    motherLastName?: string;
    identification?: IdentificationInput;
    professionalData?: ProfessionalDataInput;
    maritalStatus?: MaritalStatusInput;
    residentialAddress?: ResidentialInput;
    maxMonthlyInstallment?: number;
}

export interface MinervaFinancialUserInput {
    id?: string;
    email?: string;
    phoneNumber?: string;
    identification?: IdentificationInput;
    professionalData?: ProfessionalDataInput;
    maritalStatus?: MaritalStatusInput;
    residentialAddress?: ResidentialInput;
    maxMonthlyInstallment?: number;
}

export interface ProfessionalDataInput {
    income?: number;
}

export interface MaritalStatusInput {
    dependents?: number;
}

export interface ResidentialInput {
    address?: AddressInput;
}

export interface IdentificationInput {
    idType?: string;
    idNumber?: number;
}

export interface AddressInput {
    zipCode?: string;
    state?: string;
}

export interface ArgentinianPlayerInput {
    _id?: string;
    firstName?: string;
    lastName?: string;
    team?: string;
    dni?: number;
}

export interface BrazilianPlayerInput {
    _id?: string;
    firstName?: string;
    fastName?: string;
    injuries?: number;
    cpf?: number;
}

export interface StudentInput {
    _id?: string;
    FirstName?: string;
    LastName?: string;
    Standard?: number;
    FatherName?: string;
    MotherName?: string;
}

export interface BaseFinancialUser {
    id?: string;
    email?: string;
    phoneNumber?: string;
    identification?: Identification;
    professionalData?: ProfessionalData;
    maritalStatus?: MaritalStatus;
    residentialAddress?: Residential;
    maxMonthlyInstallment?: number;
}

export interface Player {
    _id?: string;
    firstName?: string;
    lastName?: string;
    identification?: number;
}

export interface MinervaFinancialUserOld {
    id?: string;
    income?: number;
    dependents?: number;
    zipCode?: string;
    state?: string;
}

export interface FinancialUserOld {
    id?: string;
    professionalData?: ProfessionalDataOld;
    maritalStatus?: MaritalStatusOld;
    residentialAddress?: ResidentialOld;
    maxMonthlyInstallment?: number;
}

export interface ProfessionalDataOld {
    income?: number;
}

export interface MaritalStatusOld {
    dependents?: number;
}

export interface ResidentialOld {
    address?: Address;
}

export interface AddressOld {
    zipCode?: string;
    state?: string;
}

export interface IQuery {
    getAllOldFinancialUsers(): FinancialUserOld[] | Promise<FinancialUserOld[]>;
    getOldFinancialUserById(id?: string): FinancialUserOld | Promise<FinancialUserOld>;
    getAllRefactoredFinancialUsers(): BaseFinancialUser[] | Promise<BaseFinancialUser[]>;
    getRefactoredFinancialUserById(id?: string): BaseFinancialUser | Promise<BaseFinancialUser>;
    getAllPlayers(): Player[] | Promise<Player[]>;
    getPlayerById(id?: string): Player | Promise<Player>;
    getAllStudents(): Student[] | Promise<Student[]>;
    getStudentById(id?: string): Student | Promise<Student>;
}

export interface IMutation {
    createOldFinancialUser(financialUser?: FinancialUserOldInput): FinancialUserOld | Promise<FinancialUserOld>;
    createRefactoredFinancialUser(financialUser?: FinancialUserInput): BaseFinancialUser | Promise<BaseFinancialUser>;
    createRefactoredMinervaFinancialUser(minervaFinancialUser?: MinervaFinancialUserInput): BaseFinancialUser | Promise<BaseFinancialUser>;
    createArgentinianPlayer(player?: ArgentinianPlayerInput): Player | Promise<Player>;
    createBrazilianPlayer(player?: BrazilianPlayerInput): Player | Promise<Player>;
    create(student?: StudentInput): Student | Promise<Student>;
}

export interface MinervaFinancialUser extends BaseFinancialUser {
    id?: string;
    email?: string;
    phoneNumber?: string;
    identification?: Identification;
    professionalData?: ProfessionalData;
    maritalStatus?: MaritalStatus;
    residentialAddress?: Residential;
    maxMonthlyInstallment?: number;
    campoNuevo?: string;
}

export interface FinancialUser extends BaseFinancialUser {
    id?: string;
    userProfileId?: string;
    userId?: number;
    firstName?: string;
    secondName?: string;
    lastName?: string;
    secondLastName?: string;
    gender?: string;
    email?: string;
    phoneNumber?: string;
    birthDate?: string;
    motherName?: string;
    motherLastName?: string;
    identification?: Identification;
    professionalData?: ProfessionalData;
    maritalStatus?: MaritalStatus;
    residentialAddress?: Residential;
    maxMonthlyInstallment?: number;
}

export interface ProfessionalData {
    income?: number;
}

export interface Identification {
    idType?: string;
    idNumber?: number;
}

export interface MaritalStatus {
    dependents?: number;
}

export interface Residential {
    address?: Address;
}

export interface Address {
    zipCode?: string;
    state?: string;
}

export interface ArgentinianPlayer extends Player {
    _id?: string;
    firstName?: string;
    lastName?: string;
    identification?: number;
    team?: string;
}

export interface BrazilianPlayer extends Player {
    _id?: string;
    firstName?: string;
    lastName?: string;
    identification?: number;
    injuries?: number;
}

export interface Student {
    _id?: string;
    FirstName?: string;
    LastName?: string;
    Standard?: number;
    FatherName?: string;
    MotherName?: string;
}
