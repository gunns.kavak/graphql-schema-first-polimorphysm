import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { StudentResolver } from './student/student.resolver';
import {StudentService} from './student/student.service';
import { PlayerResolver } from './player/player.resolver';
import { PlayerService } from './player/player.service';
import { FinancialUserOldResolver } from './financialUser/financialUser.resolver';
import { FinancialUserOldService } from './financialUser/financialUser.service';
import { FinancialUserRefactoredResolver } from './financialUserRefactored/financialUserRefactored.resolver';
import { FinancialUserRefactoredService } from './financialUserRefactored/financialUserRefactored.service';

@Module({
  imports: [
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      definitions:{
        path: join(process.cwd(),'src/graphql.schema.ts')
      }
    })
  ],
  controllers: [AppController],
  providers: [AppService, StudentService, StudentResolver,PlayerResolver,PlayerService,FinancialUserRefactoredResolver,FinancialUserRefactoredService,FinancialUserOldResolver,FinancialUserOldService],
})
export class AppModule {}
