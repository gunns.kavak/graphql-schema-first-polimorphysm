import { BaseFinancialUser, FinancialUser, MinervaFinancialUser } from "src/graphql.schema";

export class FinancialUserRefactoredService {
        financialUsers:BaseFinancialUser[]=[{
            id:'1',
            campoNuevo: 'new Field is here',
            maritalStatus:{dependents:1},
            maxMonthlyInstallment:40230,
            professionalData:{income:4230},
            residentialAddress:{address:{state:'Bs As',zipCode:'1252'}}
        } as MinervaFinancialUser,{
            id:'2',
            userProfileId: '1234',
            maritalStatus:{dependents:1},
            maxMonthlyInstallment:40230,
            professionalData:{income:4230},
            residentialAddress:{address:{state:'Bs As',zipCode:'1252'}}
        } as FinancialUser,{
            id:'3',
            campoNuevo: 'another new Field',
            maritalStatus:{dependents:1},
            maxMonthlyInstallment:40230,
            professionalData:{income:4230},
            residentialAddress:{address:{state:'Bs As',zipCode:'1252'}}
        } as MinervaFinancialUser];

    getAllFinancialUsers():BaseFinancialUser[]{
        console.log(JSON.stringify(this.financialUsers))
        return this.financialUsers;
    }

    getFinancialUserById(id:string):BaseFinancialUser{
        const filteredFinancialUser = this.financialUsers.filter(_ => _.id === id)[0];
        return filteredFinancialUser;
    }

    createFinancialUser(newFinancialUser:any):BaseFinancialUser{
        this.financialUsers.push(newFinancialUser);
        return newFinancialUser;
    }
}
