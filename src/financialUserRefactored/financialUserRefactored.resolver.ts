import { Resolver, Query, Args, Mutation, ResolveField } from '@nestjs/graphql';
import { BaseFinancialUser, FinancialUser, MinervaFinancialUser } from 'src/graphql.schema';
import { FinancialUserRefactoredService } from './financialUserRefactored.service';

@Resolver('BaseFinancialUser')
export class FinancialUserRefactoredResolver {
  constructor(private financialUserService: FinancialUserRefactoredService) {}

  @ResolveField()
  __resolveType(value) {
    if (value.campoNuevo) {
      return 'MinervaFinancialUser';
    }
    if(value.userProfileId) {
      return 'FinancialUser';
    }
    return null;
  }

  @Query()
  getAllRefactoredFinancialUsers(): FinancialUser[] {
    console.log('entro')
    return this.financialUserService.getAllFinancialUsers();
  }

  @Query()
  getRefactoredFinancialUserById(@Args('id') id: string):BaseFinancialUser{
      return this.financialUserService.getFinancialUserById(id);
  }

  @Mutation()
  createRefactoredMinervaFinancialUser(@Args('student')minervaFinancialUser:MinervaFinancialUser):BaseFinancialUser{
    return this.financialUserService.createFinancialUser(minervaFinancialUser);
  }

  @Mutation()
  createRefactoredFinancialUser(@Args('student')financialUser:FinancialUser):BaseFinancialUser{
    return this.financialUserService.createFinancialUser(financialUser);
  }

}
