import { Resolver, Query, Args, Mutation, ObjectType, ResolveField } from '@nestjs/graphql';
import { PlayerService } from './player.service';
import { ArgentinianPlayerInput, BrazilianPlayerInput, Player } from '../graphql.schema';

@Resolver('Player')
export class PlayerResolver {
  constructor(private playerService: PlayerService) {}

  @ResolveField()
  __resolveType(value) {
    if (value.dni) {
      return 'ArgentinianPlayer';
    }
    if(value.cpf) {
      return 'BrazilianPlayer';
    }
    return null;
  }

  @Query()
  getAllPlayers(): Player[] {
    console.log('entro')
    return this.playerService.getAllPlayers();
  }

  @Query()
  getPlayerById(@Args('id') id: string):Player{
    console.log('entro')
      return this.playerService.getPlayerById(id);
  }

  @Mutation()
  createArgentinianPlayer(@Args('student')player:ArgentinianPlayerInput):Player{
    const playerToSave = {...player, identification: player.dni}
    return this.playerService.createPlayer(playerToSave);
  }

  @Mutation()
  createBrazilianPlayer(@Args('student')player:BrazilianPlayerInput):Player{
    const playerToSave = {...player, identification: player.cpf}
    return this.playerService.createPlayer(playerToSave);
  }

}
