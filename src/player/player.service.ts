import {ArgentinianPlayer, BrazilianPlayer, Player} from '../graphql.schema';

export class PlayerService {
        players:Player[]=[{
            _id:"abc1",
            firstName: "Naveen",
            lastName: "Bommidi",
            identification: 12312312,
            cpf: 12312312,
            lesions: 2
        } as BrazilianPlayer,{
            _id:"abc2",
            firstName: "Sergio",
            lastName: "Fernandez",
            identification: 33223123,
            dni: 33223123,
            team: "Temperley"
        } as ArgentinianPlayer,{
            _id:"abc3",
            firstName: "Neymar",
            lastName: "Jr",
            identification: 23414213,
            cpf: 23414213,
            lesions: 8
        } as BrazilianPlayer];

    getAllPlayers():Player[]{
        console.log(JSON.stringify(this.players))
        return this.players;
    }

    getPlayerById(id:string):Player{
        const filteredPlayer = this.players.filter(_ => _._id === id)[0];
        return filteredPlayer;
    }

    createPlayer(newPlayer:any):Player{
        this.players.push(newPlayer);
        return newPlayer;
    }
}
