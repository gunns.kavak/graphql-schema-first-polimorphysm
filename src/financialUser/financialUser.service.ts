import { FinancialUser } from '../graphql.schema';

export class FinancialUserOldService {
        fuss:FinancialUser[]=[{
            id:'1',
            maritalStatus:{dependents:1},
            maxMonthlyInstallment:40230,
            professionalData:{income:4230},
            residentialAddress:{address:{state:'Bs As',zipCode:'1252'}}
        } ,{
            id:'2',
            maritalStatus:{dependents:2},
            maxMonthlyInstallment:2303,
            professionalData:{income:12300},
            residentialAddress:{address:{state:'Bs As',zipCode:'3000'}}
        },{
            id:'3',
            maritalStatus:{dependents:3},
            maxMonthlyInstallment:1204,
            professionalData:{income:5023},
            residentialAddress:{address:{state:'Bs As',zipCode:'1500'}}
        }];

    getAllFinancialUsers():FinancialUser[]{
        console.log(JSON.stringify(this.fuss))
        return this.fuss;
    }

    getFinancialUserById(id:string):FinancialUser{
        const filteredPlayer = this.fuss.filter(_ => _.id === id)[0];
        return filteredPlayer;
    }

    createFinancialUser(newFinancialUser:any):FinancialUser{
        this.fuss.push(newFinancialUser);
        return newFinancialUser;
    }
}
