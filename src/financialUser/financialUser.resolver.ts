import {
  Resolver,
  Query,
  Args,
  Mutation,
} from '@nestjs/graphql';
import { FinancialUserOld, FinancialUserOldInput } from 'src/graphql.schema';
import { FinancialUserOldService } from './financialUser.service';

@Resolver('FinancialUserOld')
export class FinancialUserOldResolver {
  constructor(private financialUserService: FinancialUserOldService) {}

  @Query()
  getAllOldFinancialUsers(): FinancialUserOld[] {
    return this.financialUserService.getAllFinancialUsers();
  }

  @Query()
  getOldFinancialUserById(@Args('id') id: string): FinancialUserOld {
    return this.financialUserService.getFinancialUserById(id);
  }

  @Mutation()
  createOldFinancialUser(
    @Args('student') financialUser: FinancialUserOldInput,
  ): FinancialUserOld {
    return this.financialUserService.createFinancialUser(financialUser);
  }
}
